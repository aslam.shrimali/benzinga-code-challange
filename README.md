# Steps to run

-> Install dependencies & build project for all existing packages (utils,safe-wait, subscribable, data )
use `npm install` command to install all dependencies in respective folders
use `npm run build` to build project in respective folder

-> Install dependecies in root folder (For UI)
`npm install`

-> Run react app
`npm start`

UI will load on
`http://localhost:3000`

Tech stacks:
React (hooks) + typescript
