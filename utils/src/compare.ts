export const isObject = (item: unknown): item is Record<string | number | symbol, unknown> => {
  return item != null && typeof item === 'object';
};

export const isArray = (item: unknown): item is unknown[] => {
  return Array.isArray(item);
};

export const isString = (item: unknown): item is string => {
  return typeof item === 'string' || item instanceof String;
};

export const deepEqual = <T extends unknown>(val1: T, val2: T): boolean => {
  if (isArray(val1) && isArray(val2)) {
    return arrayDeepEqual(val1, val2);
  } else if (isObject(val1) && isObject(val2)) {
    return deepEqualObject(val1 as Record<keyof T, T[keyof T]>, val2);
  } else {
    return val1 === val2;
  }
};

export const shallowEqualObject = <T extends Record<keyof T, T[keyof T]>>(object1: T, object2: T): boolean => {
  const keys1 = Object.keys(object1) as (keyof T)[];
  const keys2 = Object.keys(object2) as (keyof T)[];

  if (keys1.length !== keys2.length) {
    return false;
  }

  return keys1.every(key => object1[key] === object2[key]);
};

export const deepEqualObject = <T extends Record<keyof T, T[keyof T]>>(object1: T, object2: T): boolean => {
  const keys1 = Object.keys(object1) as (keyof T)[];
  const keys2 = Object.keys(object2) as (keyof T)[];

  if (keys1.length !== keys2.length) {
    return false;
  }

  return keys1.every(key => {
    const val1 = object1[key];
    const val2 = object2[key];
    return deepEqual(val1, val2);
  });
};

export const arrayShallowEqual = <T extends unknown[]>(array1: T, array2: T): boolean => {
  return array1.every((val1, index) => {
    const val2 = array2[index];
    return deepEqual(val1, val2);
  });
};

export const arrayDeepEqual = <T extends unknown[]>(array1: T, array2: T): boolean => {
  return array1.every((val1, index) => {
    const val2 = array2[index];
    return deepEqual(val1, val2);
  });
};
