import React, { useState, useEffect } from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Tooltip,
  TextField
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import WatchlistItems from "../Watchlist-items/index";
import AddWatchlist from "../AddWatchlist/index";
import DeleteWatchlist from "../DeleteWatchlist/index";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { toast } from "react-toastify";
import axios from "axios";
import { GetManager, Watchlist } from "@benzinga/data";
import "./style.css";
import { SymbolAutocompleteItem } from "./model";

const WatchlistComponent = () => {
  const watchListmanager = GetManager("watchlist");

  const [showModal, SetShowModal] = useState(false);
  const [showDeleteModal, SetShowDeleteModal] = useState(false);
  const [addWatchlist, setaddWatchlist] = useState("");
  const [watchlist, setWatchlist] = useState<Watchlist[]>([]);
  const [selectWatchlist, setSelectedWatchlist] = useState<Watchlist>();
  const [symbols, setSymbols] = useState<SymbolAutocompleteItem[]>([]);
  const [value, setValue] = useState<SymbolAutocompleteItem | null>(null);

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const res = await watchListmanager.createWatchlist(addWatchlist);
    if (res.result) {
      toast.success("Watchlist created successfully");
    } else {
      toast.error("Something went wrong.Please try again");
    }
    getWatchlistData();
    SetShowModal(false);
  };

  const handleDelete = async () => {
    if (selectWatchlist) {
      const res = await watchListmanager.removeWatchlist(selectWatchlist);
      if (res) {
        toast.success("Watchlist deleted successfully");
        window.location.reload();
      } else {
        toast.error("Something went wrong.Please try again");
      }
    }
  };

  const getWatchlistData = async () => {
    // this is added to avaoid auth failure when user manually reload the page as it destroy the session on page reload sometime
    const authManager = GetManager("authentication");
    await authManager.login("aslam.shrimali@gmail.com", "benzinga21");

    const res = await watchListmanager.getWatchlist();
    if (res?.result?.length) {
      setWatchlist(res?.result);
      setSelectedWatchlist(res?.result[0]);
    } else {
      setWatchlist([]);
    }
  };

  useEffect(() => {
    getWatchlistData();
  }, []);

  const getSymbolsSearch = async (data: String) => {
    const api = await axios.get(
      `https://data-api-pro.benzinga.com/rest/v2/autocomplete?apikey=aH0FkLCohY5yxK6OEaJ28Zpv51Ze1GyY&query=${data}`
    );
    if (api.data.result) {
      setSymbols(api.data.result);
    }
  };

  const addSymbol = async (symbol: SymbolAutocompleteItem | null) => {
    if (symbol && selectWatchlist) {
      setValue(symbol);
      const res = await watchListmanager.addTickerToWatchlist(
        selectWatchlist,
        symbol.symbol
      );
      if (res.result) {
        setValue(null);
        setSelectedWatchlist(undefined);
        toast.success("Symbol added successfully");
        setSelectedWatchlist(res.result);
      } else {
        toast.error("Something went wrong.Please try again");
      }
    }
  };

  return (
    <div className="container">
      <AppBar position="static" className="navbar">
        <Toolbar variant="dense">
          <Typography variant="h6">Watchlists</Typography>
          <div className="navbar-itmes">
            {selectWatchlist ? (
              <Autocomplete
                id="combo-box-demo"
                options={symbols}
                getOptionLabel={option => `${option.symbol} | ${option.name}`}
                className="searchbar"
                onChange={(event, newValue) => addSymbol(newValue)}
                value={value}
                renderInput={params => (
                  <TextField
                    {...params}
                    label="Search by symbol or security name to add.."
                    className="searchbar-input"
                    onChange={e => getSymbolsSearch(e.target.value)}
                  />
                )}
              />
            ) : (
              ""
            )}

            <div className="items">
              <Tooltip title="Create new watchlist">
                <AddIcon onClick={() => SetShowModal(!showModal)} />
              </Tooltip>
            </div>
            <div className="items">
              <Tooltip title="Delete this watchlist">
                <DeleteIcon
                  onClick={() => {
                    if (selectWatchlist) SetShowDeleteModal(!showDeleteModal);
                  }}
                />
              </Tooltip>
            </div>
            <div>
              <div className="dropdown">
                <div className="dropdown-value">
                  <span>
                    {selectWatchlist?.name
                      ? selectWatchlist.name
                      : "select watchlist"}
                  </span>
                  <KeyboardArrowDownIcon />
                </div>
                <div className="dropdown-content">
                  {watchlist &&
                    watchlist.map(singleWatchlist => (
                      <p
                        className="watchlist-values"
                        onClick={() => setSelectedWatchlist(singleWatchlist)}
                      >
                        {singleWatchlist.name}
                      </p>
                    ))}
                  <p
                    className="add-new"
                    onClick={() => SetShowModal(!showModal)}
                  >
                    <AddIcon className="watchlist-icon" />
                    Create Watchlist
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Toolbar>
      </AppBar>
      {selectWatchlist?.symbols?.length && (
        <WatchlistItems selecteWatchlist={selectWatchlist} />
      )}

      <AddWatchlist
        showJobModal={showModal}
        SetShowJobModal={SetShowModal}
        handleSubmit={handleSubmit}
        setaddWatchlist={setaddWatchlist}
      />

      {selectWatchlist && (
        <DeleteWatchlist
          showJobModal={showDeleteModal}
          SetShowJobModal={SetShowDeleteModal}
          handleDelete={handleDelete}
          selectWatchlist={selectWatchlist}
        />
      )}
    </div>
  );
};

export default WatchlistComponent;
