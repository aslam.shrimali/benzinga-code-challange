

export interface SymbolAutocompleteItem {
    currency: string,
    description: string,
    exchange: string,
    name: string,
    shortName: string
    symbol: string
    type: string
}