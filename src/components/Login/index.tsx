import React, { useState } from "react";
import {
  CssBaseline,
  Grid,
  Typography,
  Container,
  TextField,
  Button,
  CircularProgress
} from "@material-ui/core";
import "./style.css";
import { toast } from "react-toastify";
import { GetManager } from "@benzinga/data";
import { useHistory } from "react-router-dom";

const Login = () => {
  let history = useHistory();
  const [userName, setuserName] = useState<string>("");
  const [password, setpassword] = useState<string>("");
  const [isLooading, setIsLoading] = useState(false);

  const handleSubmit = async (e: React.SyntheticEvent) => {
    setIsLoading(true);
    e.preventDefault();
    const authManager = GetManager("authentication");
    const login = await authManager.login(userName, password);
    if (login.err) {
      setIsLoading(false);
      toast.error("Username or password is incorrect.");
      console.log(`Error: `, login.err);
    } else {
      console.log(`Authentication: `, login.result);
      toast.success("User logged-in successfully");
      history.push("/dashboard");
      setIsLoading(false);
    }
  };

  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      className="root"
    >
      <Grid item container justify="center">
        <Grid item sm={12} xs={11}>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Typography component="h1" variant="h4" className="login-heading">
              Sign In
            </Typography>
            <form className="login-heading" onSubmit={handleSubmit}>
              <TextField
                label="User name"
                variant="outlined"
                fullWidth
                className="login-form"
                onChange={e => setuserName(e.target.value)}
              />
              <TextField
                type="password"
                label="Password"
                variant="outlined"
                fullWidth
                className="login-form"
                onChange={e => setpassword(e.target.value)}
              />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                disabled={isLooading}
              >
                {isLooading ? "Loading..." : "Sign in"}
              </Button>
            </form>
          </Container>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Login;
