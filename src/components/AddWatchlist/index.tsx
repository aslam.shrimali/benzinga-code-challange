import React, { useState } from "react";
import CloseIcon from "@material-ui/icons/Close";
import ModalField from "../Common/ModalField";
import { Box, TextField, Button } from "@material-ui/core";
import "./style.css";

interface PhoneProps {
  showJobModal: any;
  SetShowJobModal: any;
  setaddWatchlist: any;
  handleSubmit: any;
}
const AddWatchList: React.FC<PhoneProps> = ({
  showJobModal,
  SetShowJobModal,
  handleSubmit,
  setaddWatchlist
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const handleAddWatchList = async (e: any) => {
    setIsLoading(true);
    await handleSubmit(e);
  };
  return (
    <ModalField
      showJobModal={showJobModal}
      toggleModal={SetShowJobModal}
      style="addWatchlist-modal"
    >
      <div className="modal-header">
        <div>Create A New Watchlist</div>
        <CloseIcon
          onClick={() => SetShowJobModal(false)}
          className="modal_cross"
        />
      </div>
      <Box p={4} className="emailVerify">
        <p>NEW WATCHLIST NAME</p>
        <div style={{ marginRight: "20px" }}>
          <form onSubmit={handleAddWatchList}>
            <TextField
              fullWidth
              id="outlined-basic"
              placeholder="My May 2021 Portfolio"
              variant="outlined"
              onChange={e => setaddWatchlist(e.target.value)}
            />
            <br />
            <div className="button-section">
              {!isLoading ? (
                <Button
                  type="button"
                  variant="contained"
                  className="modal-btn"
                  onClick={() => SetShowJobModal(false)}
                >
                  Cancel
                </Button>
              ) : (
                ""
              )}
              <Button
                type="submit"
                variant="contained"
                className="modal-btn1"
                disabled={isLoading}
              >
                {isLoading ? "Creating New Watch List...." : "Create watchlist"}
              </Button>
            </div>
          </form>
        </div>
      </Box>
    </ModalField>
  );
};

export default AddWatchList;
