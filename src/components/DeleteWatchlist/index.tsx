import React, { useState } from "react";
import CloseIcon from "@material-ui/icons/Close";
import ModalField from "../Common/ModalField";
import { Box, Button } from "@material-ui/core";
import "./style.css";

interface DeleteWatchlistProps {
  showJobModal: any;
  SetShowJobModal: any;
  handleDelete: any;
  selectWatchlist: any;
}
const DeleteWatchlist: React.FC<DeleteWatchlistProps> = ({
  showJobModal,
  SetShowJobModal,
  handleDelete,
  selectWatchlist
}) => {
  const [isLoading, setIsLoading] = useState(false);

  const handleAddWatchList = async (e: any) => {
    setIsLoading(true);
    await handleDelete(e);
  };
  return (
    <ModalField
      showJobModal={showJobModal}
      toggleModal={SetShowJobModal}
      style="addWatchlist-modal"
    >
      <div className="modal-header">
        <div>
          Delete <span className="text">{selectWatchlist.name}</span>
        </div>
        <CloseIcon
          onClick={() => SetShowJobModal(false)}
          className="modal_cross"
        />
      </div>
      <Box p={4} className="emailVerify">
        <p>Are you sure you want to delete {selectWatchlist.name}?</p>
        <div style={{ marginRight: "20px" }}>
          <form>
            <div className="button-section">
              {!isLoading ? (
                <Button
                  type="button"
                  variant="contained"
                  className="modal-btn"
                  onClick={() => SetShowJobModal(false)}
                >
                  Cancel
                </Button>
              ) : (
                ""
              )}
              <Button
                type="button"
                variant="contained"
                className="modal-btn1"
                onClick={handleAddWatchList}
                disabled={isLoading}
              >
                {isLoading
                  ? "Deleting Current watchlist...."
                  : "Delete watchlist"}
              </Button>
            </div>
          </form>
        </div>
      </Box>
    </ModalField>
  );
};

export default DeleteWatchlist;
