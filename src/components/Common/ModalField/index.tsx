import React from 'react';
import {
    Modal,
    Backdrop,
    Fade,
} from '@material-ui/core';

const ModelField = (props: any) => {
    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={props.style ? props.style : "job_modal"}
                open={props.showJobModal}
                onClose={() => props.toggleModal(false)}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={props.showJobModal}>
                    <div className="modal_body">
                        {props.children}
                    </div>
                </Fade>
            </Modal>
        </div>
    )
}

export default ModelField
