import React, { useState, useEffect } from "react";
import { SubscribableMultiplexer } from "@benzinga/subscribable";
import { GetManager, QuoteFeedEvent } from "@benzinga/data";

export const useFeed = (symbols: any) => {
  const quotesListmanager = GetManager("quotes");
  let subscribeMultiplexer: any;
  const [stocks, setStocks] = useState(symbols);
  let subscriptionQueue: any;

  const handleFeedUpdate = (event: any) => {
    if (event?.quote) {
      console.log(event.quote);
      const data = stocks.find((e: any) => e.symbol === event.quote.symbol);
      data["change"] = event.quote?.change;
      data["price"] = event.quote?.lastTradePrice;
      data["volume"] = event.quote?.volume;
      data["percentChange"] = event.quote?.percentChange;
      setStocks([...stocks]);
    }
  };

  useEffect(() => {
    if (stocks?.length) {
      console.log("executing:");
      if (subscribeMultiplexer) {
        stocks.forEach((singleSymbol: any) => {
          console.log("subscribing new :", singleSymbol.symbol);
          subscribeMultiplexer.remove(singleSymbol.symbol);
        });
      }

      subscribeMultiplexer = new SubscribableMultiplexer<QuoteFeedEvent>([]);

      stocks.forEach((singleSymbol: any) => {
        console.log("subscribing new :", singleSymbol.symbol);
        subscribeMultiplexer.add(
          singleSymbol.symbol,
          quotesListmanager.createQuoteFeed(singleSymbol.symbol)
        );
      });
      subscriptionQueue = subscribeMultiplexer.subscribe(
        (event: QuoteFeedEvent) => handleFeedUpdate(event)
      );
      console.log("subscribeMultiplexer new :", subscribeMultiplexer);
    }
    return () => {
      console.log("called");
      subscriptionQueue?.unsubscribe();
    };
  }, [symbols]);

  return stocks;
};
