import React, { useMemo } from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  Table,
  Grid,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper
} from "@material-ui/core";
import { useFeed } from "./getFeed";
import "./style.css";

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover
    }
  }
}))(TableRow);

interface WatchListDetailsProps {
  selecteWatchlist: any;
}

const Index: React.FC<WatchListDetailsProps> = ({ selecteWatchlist }) => {
  const stocks: any = useFeed(selecteWatchlist.symbols);

  return (
    <>
      <Grid container direction="column" justify="center" alignItems="center">
        <Grid item container justify="center">
          <Grid item sm={11} xs={11}>
            <h1>{selecteWatchlist.name}</h1>
            <TableContainer component={Paper} className="table">
              <Table aria-label="customized table">
                <TableHead>
                  <TableRow>
                    <StyledTableCell>Name</StyledTableCell>
                    <StyledTableCell align="right">Price</StyledTableCell>
                    <StyledTableCell align="right">Change($)</StyledTableCell>
                    <StyledTableCell align="right">Change(%)</StyledTableCell>
                    <StyledTableCell align="right">Volume</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {stocks.length > 0 ? (
                    stocks.map((singleSymbol: any) => (
                      <StyledTableRow key={singleSymbol.symbolId}>
                        <StyledTableCell component="th" scope="row">
                          {singleSymbol.symbol}
                        </StyledTableCell>
                        <StyledTableCell align="right">
                          {singleSymbol.price}
                        </StyledTableCell>
                        <StyledTableCell align="right">
                          {singleSymbol.change?.toFixed(2)}
                        </StyledTableCell>
                        <StyledTableCell align="right">
                          {singleSymbol.percentChange?.toFixed(2)}
                        </StyledTableCell>
                        <StyledTableCell align="right">
                          {singleSymbol.volume?.toFixed(2)}
                        </StyledTableCell>
                      </StyledTableRow>
                    ))
                  ) : (
                    <div>
                      <h3>No symbols are found.</h3>
                    </div>
                  )}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
export default Index;
